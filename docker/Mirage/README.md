# Mirage-Baremetal-RiscV

---

This repository demonstrates the steps required to build and simulate Mirage
unikernels on baremetal RISC-V

## What

Prints "Hello!" every second to demonstrate the most basic Mirage unikernel.

## How

```
# Run the container
docker run -it kayceesrk/riscv-mirage-baremetal:0.1.0
# Make some changes to unikernel.ml and build.
eval $(opam env)
mirage configure -t riscv
make depends
mirage build
make kernel
# Run the kernel
spike kernel
```

#### Note

Currently the `mirage build` generates the `main.native.o` of RISC-V and does
not link anything further. The linking step is taken care of `make kernel` which
generates `rv64` ELF file, `kernel` which can be simulated on
[spike](https://gitlab.com/shaktiproject/tools/shakti-tee/shakti-tee-isa-sim)

The packages ported out in `opam-cross-shakti` custom repository support
building simple unikernel applications like `hello-world` . Further packages to
support other applications are being ported out.
