# prebuilt directory

This directory should contain the ocaml object file compiled with 
`$ ocamlopt -ccopt -static -output-obj -o payload.o file.ml`
where `file.ml` is the payload file.

It is important that the prefix for the output file is different from the input file otherwise the ocaml compiler has some hickup's while writing `payload.o`.
The object file _MUST_ have the name `payload.o`.

An example file is provided in `example.o`
